'use strict';
var NoteObject = (function () {
    function NoteObject(note_id, note_name, note_content, note_synched) {
        this.note_name = note_name;
        this.note_id = note_id;
        this.note_content = note_content;
        this.note_synched = note_synched;
    }
    Object.defineProperty(NoteObject.prototype, "noteId", {
        get: function () {
            return this.note_id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NoteObject.prototype, "noteContent", {
        get: function () {
            return this.note_content;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NoteObject.prototype, "noteName", {
        get: function () {
            return this.note_name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NoteObject.prototype, "noteSynched", {
        get: function () {
            return this.note_synched;
        },
        enumerable: true,
        configurable: true
    });
    return NoteObject;
})();
var maxId = 0;
var db;
var dialog;
if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("/app-sw.js").then(function (success) {
    }).catch(function (error) {
        console.log("nope!", error);
    });
}
$(document).ready(function () {
    fetchNotes();
    dialog = document.querySelector('dialog');
    $("#show-dialog").click(function (event) {
        event.preventDefault();
        dialog.showModal();
    });
    // not working!
    if (!dialog.showModal) {
        dialogPolyfill.registerDialog(dialog);
    }
    dialog.querySelector('.close').addEventListener('click', function () {
        dialog.close();
    });
    $("#create-button").click(function (event) {
        var noteName = $("#note-name-field").val();
        var noteContent = $("#note-content-field").val();
        var postObject = new NoteObject(maxId, noteName, noteContent, false);
        var postArray = new Array();
        postArray.push(postObject);
        postNotes(postArray, true);
    });
});
/**
 * This method posts an array of notes to note service
 */
function postNotes(postArray, fromCreate) {
    // execute ajax opnly when post array contains at least one element
    if (postArray.length > 0) {
        jQuery.ajax({
            type: "POST",
            url: 'http://localhost:9999/notes/add',
            crossDomain: true,
            data: JSON.stringify(postArray)
        }).done(function (response) {
<<<<<<< HEAD
            $(".page-content").empty();
            populateNotes(response);
            restoreView();
        }).fail(function (response) {
            console.log(response);
            maxId++;
            addDataToDb(postObject);
=======
            if (response.indexOf("offline") >= 0) {
                if (fromCreate) {
                    addDataToDb(postArray[0]);
                }
                else {
                    // when multiple fail then only put them to the dom because they are already in the database
                    for (var _i = 0, postArray_1 = postArray; _i < postArray_1.length; _i++) {
                        var obj = postArray_1[_i];
                        addNoteToDOM(obj, maxId);
                        maxId++;
                    }
                }
            }
            else {
                // wipe all data from database - db was opened before
                fetchNotes();
                var objectStore = db.transaction(["notes"], "readwrite").objectStore("notes");
                objectStore.clear();
            }
            //readNotesFromDb();
>>>>>>> e9a33a05c68d8f0fcd31a94362142531be124ae0
            restoreView();
        });
    }
}
/**
 * add notes coming from response;
 */
function populateNotes(response) {
    for (var i = 0; i < response.notes.length; i++) {
        var responseNote = response.notes[i];
        addNoteToDOM(new NoteObject(responseNote.note_id, responseNote.note_name, responseNote.note_content, responseNote.note_synched), i);
    }
    maxId = response.notes.length;
}
/**
 * puts note reprensentation to DOM structure
 */
function addNoteToDOM(noteObject, index) {
    var noteTemplate = $("#note-template").clone();
    noteTemplate.find(".mdl-card__title-text")[0].textContent = noteObject.noteName;
    noteTemplate.find(".mdl-card__supporting-text")[0].textContent = noteObject.noteContent;
    if (noteObject.noteSynched) {
        noteTemplate.find(".mdl-card__title")[0].classList.add("note-synched");
    }
    else {
        noteTemplate.find(".mdl-card__title")[0].classList.add("note-unsynched");
    }
    noteTemplate.attr("id", "card-" + index);
    if ($(".page-content").children().length == 0) {
        $(".page-content").append(noteTemplate);
    }
    else {
        noteTemplate.insertBefore("#card-" + (index - 1));
    }
}
/**
 * fetches all notes
 */
function fetchNotes() {
    $(".page-content").empty();
    jQuery.ajax({
        url: 'http://localhost:9999/notes/all',
        crossDomain: true,
        dataType: 'json'
    }).done(function (response) {
        // after fetch add responses to DOM when online or not
        populateNotes(response);
        // get data from database and try to post pending notes
        openDatabase();
    });
}
/**
 * opens database and defines success methods
 */
function openDatabase() {
    var openResponse = indexedDB.open("outbox", 1);
    // database is a new one
    openResponse.onupgradeneeded = function (e) {
        db = e.target.result;
        if (!db.objectStoreNames.contains("notes")) {
            var objectStore = db.createObjectStore("notes", { keyPath: "note_id" });
            objectStore.createIndex("note_name", "note_name", { unique: false });
            objectStore.createIndex("note_content", "note_content", { unique: false });
        }
    };
    openResponse.onsuccess = function (e) {
        db = e.target.result;
        // try to post all pending notes
        readNotesFromDb();
    };
}
/**
 * reads notes from database
 */
function readNotesFromDb() {
    var objectStore = db.transaction(["notes"]).objectStore("notes");
    objectStore.openCursor().onsuccess = function (event) {
        var pendingNotes = new Array();
        var cursor = event.target.result;
        if (cursor) {
            pendingNotes.push(new NoteObject(cursor.value.note_id, cursor.value.note_name, cursor.value.note_content, false));
            cursor.continue();
        }
        postNotes(pendingNotes, false);
    };
}
function addDataToDb(postObject) {
    if (db) {
        var transaction = db.transaction(["notes"], "readwrite");
        var objectStore = transaction.objectStore("notes");
        objectStore.add(postObject);
        addNoteToDOM(postObject, maxId);
    }
}
function restoreView() {
    dialog.close();
    $("#note-name-field").val("");
    $("#note-content-field").val("");
}
//# sourceMappingURL=app.js.map
var resourcesToCache = [
    '/resources/style.css',
    '/resources/app.js',
    '/index.html',
    '/thirdparty/jquery/jquery.js',
    '/thirdparty/mdl/material.js',
    '/thirdparty/mdl/material.min.css',
    'http://localhost:9999/notes/all'/*,
    'https://fonts.googleapis.com/icon?family=Material+Icons'*/
];

var CACHE_NAME = "my-app-cache";

self.addEventListener("install", function (e) {
    e.waitUntil(
        caches.open(CACHE_NAME).then(function (cache) {
            console.log("Opened cache");
            return cache.addAll(resourcesToCache);
        })
    );
});

self.addEventListener("fetch", function (e) {
    e.respondWith(
        caches.match(e.request).then(function (cachedResponse) {
            if (cachedResponse && e.request.url.indexOf("notes/all") < 0) {
                // the cachedResponse from cache returned - exclude notes/all
                return cachedResponse;
            }

            var fetchRequest = e.request.clone();

            // fetch if necessary
            return fetch(fetchRequest).then(
                function (response) {
                    // valid response?
                    if (!response || response.status !== 200 || e.request.method === "POST") {
                        return response;
                    }
                    
                    var responseToCache = response.clone();
                    caches.open(CACHE_NAME).then(function (cache) {
                        cache.put(e.request, responseToCache);
                    })

                    return response;
                }
                , function (response) {
                    if (fetchRequest.url.indexOf("notes/add") > 0) {
                        var blob = new Blob(["offline"],{"type":'text/html'});
                        var init = { "status": 202, "statusText": "offline"};
                        var response = new Response(blob, init);
                        return response;
                    }

                    if (cachedResponse && e.request.url.indexOf("notes/all") > 0)
                    {
                        return cachedResponse;
                    }
                }
            );
        })
    );
});

function addPendingNote(params) {
    pendingNotes.push(params);
}